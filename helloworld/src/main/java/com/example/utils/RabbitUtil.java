package com.example.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author hanj.cn@outlook.com
 * @date 2022/1/7 12:11
 */
public class RabbitUtil {

    public final static String QUEUE_NAME = "queue-hello";

    public static Channel getChannel() {
        //创建一个连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.87.141");
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setPort(5673);
        //channel 实现了自动 close 接口 自动关闭 不需要显示关闭
        Connection connection = null;
        try {
            connection = factory.newConnection();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
        try {
            assert connection != null;
            return connection.createChannel();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void sleep(int seconds) {

        try {
            Thread.sleep(1000L * seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
