package com.example.confirm;

import com.example.utils.RabbitUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmCallback;

import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * @author hanj.cn@outlook.com
 * @date 2022/1/7 12:09
 */
public class ConfirmMessage {

    /**
     * 消息总数
     */
    private static final int MESSAGE_COUNTER = 1000;
    /**
     * 信道
     */
    private static final Channel CHANNEL = RabbitUtil.getChannel();

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) throws Exception {

        publishMessageIndividually();
        publishMessageBatch();
        publishMessageAsynchronous();
    }

    /**
     * 单个确认
     */
    public static void publishMessageIndividually() throws Exception {

        String queueName = UUID.randomUUID().toString().substring(0, 8);
        System.out.println("queueName:" + queueName);
        /*
         形参名
         1.队列名称
         2.队列里面的消息是否持久化 默认消息存储在内存中
         3.该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费
         4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
         5.其他参数
        */
        assert CHANNEL != null;
        CHANNEL.queueDeclare(queueName, true, false, true, null);
        // 开启发布确认
        CHANNEL.confirmSelect();
        // 开始时间
        long begin = System.currentTimeMillis();

        // 开始批量发消息
        for (int i = 0; i < MESSAGE_COUNTER; i++) {
            String message = i + "";
            CHANNEL.basicPublish("", queueName, null, message.getBytes(StandardCharsets.UTF_8));
            // 发布后就马上进行发布确认
            CHANNEL.waitForConfirms();
        }
        // 结束时间
        long end = System.currentTimeMillis();
        System.out.println("单个发布确认:发布" + MESSAGE_COUNTER + "条消息共耗时" + (end - begin) + "毫秒");
    }

    /**
     * 批量确认
     */
    public static void publishMessageBatch() throws Exception {

        String queueName = UUID.randomUUID().toString().substring(0, 8);
        System.out.println("queueName:" + queueName);
        /*
         形参名
         1.队列名称
         2.队列里面的消息是否持久化 默认消息存储在内存中
         3.该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费
         4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
         5.其他参数
        */
        assert CHANNEL != null;
        CHANNEL.queueDeclare(queueName, true, false, true, null);
        // 开启发布确认
        CHANNEL.confirmSelect();
        // 开始时间
        long begin = System.currentTimeMillis();
        // 批量的容量
        int capacity = (int) (MESSAGE_COUNTER * 0.1);
        // 开始批量发消息
        for (int i = 1; i <= MESSAGE_COUNTER; i++) {
            String message = i + "";
            CHANNEL.basicPublish("", queueName, null, message.getBytes(StandardCharsets.UTF_8));
            // 批量进行发布确认
            if (i % 100 == 0) {
                CHANNEL.waitForConfirms();
            }
        }
        // 结束时间
        long end = System.currentTimeMillis();
        System.out.println("批量发布确认:发布" + MESSAGE_COUNTER + "条消息共耗时" + (end - begin) + "毫秒");
    }

    /**
     * 异步确认发布
     */
    public static void publishMessageAsynchronous() throws Exception {

        ConcurrentSkipListMap<Long, String> outStandingContainer = new ConcurrentSkipListMap<>();
        String queueName = UUID.randomUUID().toString().substring(0, 8);
        System.out.println("queueName:" + queueName);
        /*
         形参名
         1.队列名称
         2.队列里面的消息是否持久化 默认消息存储在内存中
         3.该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费
         4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
         5.其他参数
        */
        assert CHANNEL != null;
        CHANNEL.queueDeclare(queueName, true, false, true, null);
        // 开启发布确认
        CHANNEL.confirmSelect();
        // 开始时间
        long begin = System.currentTimeMillis();
        // 消息确认成功的回调函数
        ConfirmCallback ackCallback = (deliveryTag, multiple) -> {
            if (multiple) {
                // 删除(批量确认的,成功的) 所有已经确认的消息,因为他们的deliveryTag会连续的相同
                ConcurrentNavigableMap<Long, String> confirmedMap = outStandingContainer.headMap(deliveryTag);
                confirmedMap.clear();
            } else {
                // 删除(单个确认的,成功的) 所有已经确认的消息
                outStandingContainer.remove(deliveryTag);
            }

        };
        // 消息确认失败的回调函数
        ConfirmCallback nackCallback = (deliveryTag, multiple) -> System.out.println("message:" + outStandingContainer.get(deliveryTag) + " messageTag:" + deliveryTag + "确认失败");
        // 定义消息的监听器
        CHANNEL.addConfirmListener(ackCallback, nackCallback);
        // 开始批量发消息
        for (int i = 1; i <= MESSAGE_COUNTER; i++) {
            String message = i + "";
            // 记录所有的消息
            CHANNEL.basicPublish("", queueName, null, message.getBytes(StandardCharsets.UTF_8));
            outStandingContainer.put(CHANNEL.getNextPublishSeqNo() - 1, message);

        }
        // 结束时间
        long end = System.currentTimeMillis();
        System.out.println("异步发布确认:发布" + MESSAGE_COUNTER + "条消息共耗时" + (end - begin) + "毫秒");
    }

}
