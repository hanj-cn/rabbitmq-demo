package com.example.receive;

import com.example.utils.RabbitUtil;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * 发送者
 *
 * @author hanj.cn @outlook.com
 * @date 2022 /1/7 11:53
 */
public class SanderA {

    /**
     * 交换机名字
     */
    public static final String EXCHANGE_NAME = "logs";

    public static void main(String[] args) throws IOException {

        String routingKey = "my routing key";
        String exchangeType = "fanout";
        // 拿到信道
        Channel channel = RabbitUtil.getChannel();
        // 绑定交换机
        assert channel != null;
        channel.exchangeDeclare(EXCHANGE_NAME, exchangeType);
        Scanner scanner = new Scanner(System.in);
        System.out.println("在下方输入消息: ");
        while (scanner.hasNext()) {
            String message = scanner.next();
            channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes(StandardCharsets.UTF_8));

            System.out.println("发送: " + message);
        }
    }

}
