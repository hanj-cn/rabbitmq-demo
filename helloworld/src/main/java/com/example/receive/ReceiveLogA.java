package com.example.receive;

import com.example.utils.RabbitUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author hanj.cn@outlook.com
 * @date 2022/1/7 11:44
 */
public class ReceiveLogA {

    public static final String EXCHANGE_NAME = "logs";

    public static void main(String[] args) throws IOException {

        Channel channel = RabbitUtil.getChannel();
        // 声明交换机
        assert channel != null;
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        // 生成一个临时队列
        String queueName = channel.queueDeclare().getQueue();
        // 交换机与队列绑定
        String routingKey = "key1";
        channel.queueBind(queueName, EXCHANGE_NAME, routingKey);
        System.out.println("waiting for message... it will be printed on screen");

        // 接收消息时的回调函数
        DeliverCallback deliverCallback = (consumerTag, message) -> System.out.println("控制台打印接收到的消息: " + new String(message.getBody(), StandardCharsets.UTF_8));
        // 消费者取消消息时的回调接口
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
        });
    }

}
