package com.example.worker;

import com.example.utils.RabbitUtil;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.util.UUID;

/**
 * @author hanj.cn@outlook.com
 * @date 2022/1/7 12:09
 */
public class WorkerA {

    public static void main(String[] args) throws Exception {

        String id = UUID.randomUUID().toString().substring(0, 8);
        Channel channel = RabbitUtil.getChannel();
        System.out.println("worker : " + id + " 等待接收消息...");

        //推送的消息如何进行消费的接口回调
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody());
            System.out.println("消息 = " + message);
        };

        //取消消费的一个回调接口 如在消费的时候队列被删除掉了
        CancelCallback cancelCallback = (consumerTag) -> System.out.println("消息消费被中断");

        /*
          消费者消费消息
          1.消费哪个队列
          2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
          3.消费者成功消费的回调
         */
        assert channel != null;
        channel.basicConsume(RabbitUtil.QUEUE_NAME, true, deliverCallback, cancelCallback);
    }

}