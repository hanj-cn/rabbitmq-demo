package com.example.provider;

import com.example.utils.RabbitUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;

import java.util.Scanner;

/**
 * @author hanj.cn@outlook.com
 * @date 2022/1/7 12:11
 */
public class ProviderA {

    public static void main(String[] args) throws Exception {
        // 是否持久化
        boolean isDurable = true;
        Channel channel = RabbitUtil.getChannel();
        // 开启发布确认
        assert channel != null;
        channel.confirmSelect();
        /*
          生成一个队列
          1.队列名称
          2.队列里面的消息是否持久化 默认消息存储在内存中
          3.该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费
          4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
          5.其他参数
         */
        channel.queueDeclare(RabbitUtil.QUEUE_NAME, isDurable, false, false, null);
        Scanner scanner = new Scanner(System.in);
        System.out.println("enterBelow: ");
        while (scanner.hasNextLine()) {
            String message = scanner.next();
            channel.basicPublish("", RabbitUtil.QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
            System.out.println("send message: " + message);

        }
    }

}
